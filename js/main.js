const mobileBtn = document.querySelector(".mobile-menu-toggle"),
      mobileClose = document.querySelector(".mobile-menu-close"),
      body = document.querySelector("body"),
      mobileMenu = document.querySelector(".header-nav");

mobileBtn.addEventListener("click", function(){
    mobileMenu.classList.add("header-nav_visible");
    body.classList.add("no-scroll");
});

mobileClose.addEventListener("click", function(){
    mobileMenu.classList.remove("header-nav_visible");
    body.classList.remove("no-scroll");
})